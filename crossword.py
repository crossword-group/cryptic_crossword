# Code for the cryptic crossword problem

# Function 1: traversing through the list -> (a,b, x/y)
# Function 2: checking for across word -> x
# Function 3: checking for down word -> y 

GRID = '''        #       # # # ### # #         #       # # # # # # #            #### # ### # # # #      #          # # # # # # #          #      # # # # ### # ####            # # # # # # #       #         # # ### # # #       #        '''

SIZE = 15
WHITE = " "
BLACK = "#"

def is_down(position: int) -> bool:
    if position >= SIZE:
        if (position + SIZE < len(GRID) and GRID[position + SIZE] == WHITE) and GRID[position - SIZE] == BLACK:
            return True
    else:
        if GRID[position + SIZE] == WHITE:
            return True
    return False


def is_across(position: int) -> bool:
    if position % SIZE == 0:
        return GRID[position + 1] == WHITE
    elif position % SIZE == SIZE - 1:
        return False
    else:
        return GRID[position - 1] == BLACK and GRID[position + 1] == WHITE


def number_crossword(grid: str) -> list[tuple[int, int, int]]:
    clue_numbers = []
    current_clue = 1
    for position in range(0, len(GRID)):
        if GRID[position] == WHITE:
            if is_across(position) or is_down(position):
                clue_numbers.append((position // SIZE, position % SIZE, current_clue))
                current_clue += 1
    return clue_numbers

print(number_crossword(GRID))
