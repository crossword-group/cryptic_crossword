#cryptic_crossword

# Observations from the crossword:

1. numbers are in ascending order
2. row- wise
3. down clues: the column to the right is either occupied or not present
4. clue 1: always at box (1,1)
5. across: find length of word in the across that is after the previous number and isn't occupied


# Functions:

1. check for across: right box: white
2. check for down: above and below: up=black and down=white
3. assigning the clue number to the box coordinates

L[row][column]
