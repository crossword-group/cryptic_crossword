WHITE = ' '
BLACK = '#'
SIZE = 15
PADDED_SIZE = 17
BWW = BLACK + WHITE + WHITE

inp = '''
        #      
 # # # ### # # 
        #      
 # # # # # # # 
           ####
 # ### # # # # 
     #         
 # # # # # # # 
         #     
 # # # # ### # 
####           
 # # # # # # # 
      #        
 # # ### # # # 
      #        
'''


def read_grid(inp: str) -> list[str]:
    return [_ for _ in inp.split("\n") if _]


def pad(grid: list[str]) -> list[str]:
    ALL_BLACKS = BLACK * SIZE
    grid = [ALL_BLACKS] + grid + [ALL_BLACKS]
    return [BLACK + row + BLACK for row in grid]


def transpose(grid: list[str]) -> list[str]:
    return [''.join(_) for _ in zip(*grid)]


def clue_positions(cells: str) -> list[int]:
    clue_positions = [cells.find(BWW)]
    while (next_clue := cells.find(BWW, clue_positions[-1] + 1)) != -1:
        clue_positions.append(next_clue)
    return clue_positions


def unflatten_single(clue: int) -> tuple[int, int]:
    row, col = divmod(clue, PADDED_SIZE)
    return row - 1, col


def unflatten(clues: list[int]) -> list[tuple[int, int]]:
    return [unflatten_single(item) for item in clues]


def number_crossword(inp: str) -> list[tuple[int, int, int]]:
    grid = read_grid(inp)
    padded_grid = pad(grid)
    flattened_rowwise = ''.join(padded_grid)
    flattened_colwise = ''.join(transpose(padded_grid))
    rowwise_clues = unflatten(clue_positions(flattened_rowwise))
    columnwise_clues = [clue[::-1] for clue in unflatten((clue_positions(flattened_colwise)))]
    all_clues = set(rowwise_clues) | set(columnwise_clues)
    sorted_clues = sorted(list(all_clues), key = lambda clue: 100 * clue[0] + clue[1])
    return list(enumerate(sorted_clues, start = 1))


print(number_crossword(inp))
